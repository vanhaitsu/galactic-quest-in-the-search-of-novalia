using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    Vector2 startPos;
    Rigidbody2D playerRb;
    private KeyManager km;
    private QuestionManager qm;

    private void Awake()
    {
        playerRb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        startPos = transform.position;
        km = FindObjectOfType<KeyManager>();
        qm = FindObjectOfType<QuestionManager>();

        if (km == null)
        {
            Debug.LogError("KeyManager not found! Please assign it in the Inspector or make sure there is a KeyManager in the scene.");
        }

        if (qm == null)
        {
            Debug.LogError("QuestionManager not found! Please assign it in the Inspector or make sure there is a QuestionManager in the scene.");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Obstracle"))
        {
            Die();
        }
        else if (collision.CompareTag("Key"))
        {
            KeyQuestion keyQuestion = collision.GetComponent<KeyQuestion>();
            if (keyQuestion != null && qm != null)
            {
                qm.ShowQuestion(keyQuestion.question, keyQuestion.answers, keyQuestion.correctAnswer, collision.gameObject, this);
            }
        }
    }

    public void CorrectAnswer(GameObject key)
    {
        if (km != null)
        {
            km.keyCount++;
        }
        Destroy(key);
    }

    public void IncorrectAnswer()
    {
        Die();
    }

    public void Die()
    {
        StartCoroutine(Respawn(0.5f));
    }

    IEnumerator Respawn(float duration)
    {
        playerRb.simulated = false;
        playerRb.velocity = Vector2.zero;
        transform.localScale = Vector3.zero;
        yield return new WaitForSeconds(duration);
        transform.position = startPos;
        transform.localScale = Vector3.one;
        playerRb.simulated = true;
    }
}
