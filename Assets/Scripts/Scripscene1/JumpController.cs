﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpController : MonoBehaviour
{
    Rigidbody2D rb;

    [Header("Jump System")]
    [SerializeField] float jumpTime; // Thời gian nhảy
    [SerializeField] int jumpPower; // Lực nhảy
    [SerializeField] float fallMultiplier; // Tốc độ rơi
    [SerializeField] float jumpMultiplier; // Tốc độ tăng khi nhảy

    public Transform groundCheck;
    public LayerMask groundLayer;

    private bool isJumping;
    private float jumpCounter;
    private Vector2 vecGravity;

    private float originalGravityScale;
    private bool isOnPlatform;

    void Start()
    {
        vecGravity = new Vector2(0, Physics2D.gravity.y); // Sửa lại hướng của vector trọng lực
        rb = GetComponent<Rigidbody2D>();
        originalGravityScale = rb.gravityScale; // Store the original gravity scale
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) && isGrounded())
        {
            rb.gravityScale = originalGravityScale; // Ensure gravity scale is normal during jumps
            rb.velocity = new Vector2(rb.velocity.x, jumpPower);
            isJumping = true;
            jumpCounter = 0;
        }

        if (rb.velocity.y > 0 && isJumping)
        {
            jumpCounter += Time.deltaTime;
            if (jumpCounter > jumpTime)
            {
                isJumping = false;
            }
            float t = jumpCounter / jumpTime;
            float currentJumpMultiplier = jumpMultiplier;
            if (t > 0.5f)
            {
                currentJumpMultiplier = jumpMultiplier * (1 - t);
            }
            rb.velocity += vecGravity * currentJumpMultiplier * Time.deltaTime;
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            isJumping = false;
            jumpCounter = 0;
            if (rb.velocity.y > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.6f);
            }
        }

        if (rb.velocity.y < 0)
        {
            rb.velocity += vecGravity * fallMultiplier * Time.deltaTime;
        }
    }

    bool isGrounded()
    {
        return Physics2D.OverlapCapsule(groundCheck.position, new Vector2(0.66f, 0.13f), CapsuleDirection2D.Horizontal, 0, groundLayer);
    }

    public void SetOnPlatform(bool onPlatform)
    {
        isOnPlatform = onPlatform;
        rb.gravityScale = onPlatform ? originalGravityScale * 50 : originalGravityScale;
    }
}
