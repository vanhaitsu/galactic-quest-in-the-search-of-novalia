﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KeyManager : MonoBehaviour
{
    public int keyCount;
    public TMP_Text keyText;
    public GameObject Door;
    private bool doorDestroyed;

    public int requiredKeyCount;

    private void Awake()
    {
        // Đặt lại keyCount khi cảnh được tải
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void Start()
    {
        UpdateKeyText();
    }

    void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void Update()
    {
        UpdateKeyText();

        if (keyCount == requiredKeyCount && !doorDestroyed)
        {
            Debug.Log($"Key count is {requiredKeyCount}. Destroying the door.");
            doorDestroyed = true;

            if (Door != null)
            {
                Destroy(Door);
            }
            else
            {
                Debug.LogError("Door GameObject is not assigned.");
            }
        }
    }

    void UpdateKeyText()
    {
        if (keyText != null)
        {
            keyText.text = keyCount.ToString() + $"/{requiredKeyCount}";
        }
        else
        {
            Debug.LogError("keyText is not assigned.");
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        InitializeForScene(scene);
    }

    private void InitializeForScene(Scene scene)
    {
        ResetKeyCount();

        // Set the required key count based on the scene
        if (scene.name == "Level 1")
        {
            requiredKeyCount = 3;
        }
        else if (scene.name == "Level 2")
        {
            requiredKeyCount = 6;
        }
        else if (scene.name == "Level 3")
        {
            requiredKeyCount = 6;
        }
        else if (scene.name == "Level 4")
        {
            requiredKeyCount = 6;
        }
        else
        {
            requiredKeyCount = 0; // Default value or handle other scenes accordingly
        }

        UpdateKeyText();
    }

    public void ResetKeyCount()
    {
        keyCount = 0;
        doorDestroyed = false;
    }
}
