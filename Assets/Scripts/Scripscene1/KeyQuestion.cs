using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyQuestion : MonoBehaviour
{
    public string question;
    public string[] answers;
    public string correctAnswer;
}
