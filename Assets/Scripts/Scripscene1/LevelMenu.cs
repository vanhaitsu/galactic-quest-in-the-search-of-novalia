﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMenu : MonoBehaviour
{
    public Button[] buttons;

    private void Awake()
    {
        int unlockedLevel = PlayerPrefs.GetInt("UnlockedLevel", 1);

        Debug.Log("Unlocked Level: " + unlockedLevel);

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = false;
        }

        for (int i = 0; i < buttons.Length && i < unlockedLevel; i++)
        {
            buttons[i].interactable = true;
            Debug.Log("Button " + i + " is now interactable.");
        }
        for (int i = 0; i < buttons.Length; i++)
        {
            int levelId = i + 1;
            buttons[i].onClick.AddListener(() => OpenLevel(levelId));
        }
    }

    public void OpenLevel(int levelId)
    {
        string levelName = "Level " + levelId;

        Debug.Log("Attempting to load scene: " + levelName);

        if (Application.CanStreamedLevelBeLoaded(levelName))
        {
            SceneManager.LoadScene(levelName);
        }
        else
        {
            Debug.LogError("Scene " + levelName + " cannot be loaded. Check if the scene is added to the Build Settings.");
        }
    }

    public void ResetLevels()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("UnlockedLevel", 1); 
        PlayerPrefs.Save();
        Debug.Log("Game progress has been reset.");
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = i == 0; 
        }
    }
}
