﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController2 : MonoBehaviour
{
    Vector3 move;
    public float speed ; 
    Rigidbody2D rb;
    bool facingRight = true;

    public bool isOnPlatform;
    public Rigidbody2D platformRB;

    public Vector3 lastPlatformPosition;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        // Capture input in Update
        move = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        flip();
    }

    void FixedUpdate()
    {
        if (isOnPlatform && platformRB != null)
        {
            Vector3 deltaPosition = platformRB.transform.position - lastPlatformPosition;
            transform.position += deltaPosition;
        }

        rb.velocity = new Vector2(move.x * speed, rb.velocity.y);

        if (isOnPlatform && platformRB != null)
        {
            lastPlatformPosition = platformRB.transform.position;
        }
    }

    void flip()
    {
        if (move.x > 0 && !facingRight)
        {
            facingRight = true;
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (move.x < 0 && facingRight)
        {
            facingRight = false;
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }
}
