﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public float speed;
    private Vector3 targetPos;
    private Rigidbody2D rb;
    private Vector3 moveDirection;

    public GameObject ways;
    public Transform[] wayPoints;
    private int pointIndex;
    private int pointCount;
    private int direction = 1;
    private Rigidbody2D playerRb;
    private float originalGravityScale;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();

        // Initialize waypoints from children of 'ways' GameObject
        int wayPointCount = ways.transform.childCount;
        wayPoints = new Transform[wayPointCount];
        for (int i = 0; i < wayPointCount; i++)
        {
            wayPoints[i] = ways.transform.GetChild(i);
        }

        playerRb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
        if (playerRb != null)
        {
            originalGravityScale = playerRb.gravityScale;
        }
        else
        {
            Debug.LogError("Player Rigidbody2D not found!");
        }
    }

    void Start()
    {
        pointIndex = 1;
        pointCount = wayPoints.Length;
        targetPos = wayPoints[pointIndex].position;
        CalculateDirection();
    }

    void Update()
    {
        if (Vector2.Distance(transform.position, targetPos) < 0.05f)
        {
            NextPoint();
        }
    }

    private void FixedUpdate()
    {
        rb.velocity = moveDirection * speed;
    }

    private void NextPoint()
    {
        transform.position = targetPos;
        if (pointIndex == pointCount - 1)
        {
            direction = -1;
        }
        else if (pointIndex == 0)
        {
            direction = 1;
        }
        pointIndex += direction;
        targetPos = wayPoints[pointIndex].position;
        CalculateDirection();
    }

    private void CalculateDirection()
    {
        moveDirection = (targetPos - transform.position).normalized;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            MovementController2 playerController = collision.GetComponent<MovementController2>();
            if (playerController != null)
            {
                playerController.isOnPlatform = true;
                playerController.platformRB = rb;
                playerController.lastPlatformPosition = transform.position;
                playerRb.gravityScale = originalGravityScale * 50;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            MovementController2 playerController = collision.GetComponent<MovementController2>();
            if (playerController != null)
            {
                playerController.isOnPlatform = false;
                playerController.platformRB = null;
                playerRb.gravityScale = originalGravityScale;
            }
        }
    }
}
