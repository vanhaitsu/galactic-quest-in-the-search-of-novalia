using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuestionManager : MonoBehaviour
{
    public GameObject questionPanel;
    public TMP_Text questionText;
    public Button[] answerButtons;

    private string correctAnswer;
    private GameObject currentKey;
    private GameController gameController;

    void Start()
    {
        questionPanel.SetActive(false);

        for (int i = 0; i < answerButtons.Length; i++)
        {
            int index = i;
            answerButtons[i].onClick.AddListener(() => OnAnswerSelected(index));
        }
    }

    public void ShowQuestion(string question, string[] answers, string correctAnswer, GameObject key, GameController controller)
    {
        this.correctAnswer = correctAnswer;
        this.currentKey = key;
        this.gameController = controller;

        questionText.text = question;
        for (int i = 0; i < answers.Length; i++)
        {
            answerButtons[i].GetComponentInChildren<TMP_Text>().text = answers[i];
        }

        questionPanel.SetActive(true);
        Time.timeScale = 0f;
    }

    private void OnAnswerSelected(int index)
    {
        string selectedAnswer = answerButtons[index].GetComponentInChildren<TMP_Text>().text;

        questionPanel.SetActive(false);
        Time.timeScale = 1f;

        if (selectedAnswer == correctAnswer)
        {
            gameController.CorrectAnswer(currentKey);
        }
        else
        {
            gameController.IncorrectAnswer();
        }
    }
}
