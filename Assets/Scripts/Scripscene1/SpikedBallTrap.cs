using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikedBallTrap : MonoBehaviour
{
    private GameController gameController;

    void Start()
    {
        // Find the GameController in the scene
        gameController = FindObjectOfType<GameController>();
        if (gameController == null)
        {
            Debug.LogError("GameController not found! Please ensure there is a GameController in the scene.");
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (gameController != null)
            {
                gameController.Die();
            }
        }
    }
}
